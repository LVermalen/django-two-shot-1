from django.shortcuts import render
from receipts.models import Receipt, ExpenseCategory, Account
from django.views.generic import ListView, CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy

# Create your views here.


class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "receipts/list.html"
    context_object_name = "receipt_list"

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)


class ReceiptCreateView(LoginRequiredMixin, CreateView):
    model = Receipt
    fields = ["vendor", "total", "tax", "date", "category", "account"]
    template_name = "receipts/new.html"
    success_url = reverse_lazy("home")

    def form_valid(self, form):
        form.instance.purchaser = self.request.user
        return super().form_valid(form)

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        form.fields["account"].queryset = Account.objects.filter(
            owner=self.request.user
        )
        form.fields["category"].queryset = ExpenseCategory.objects.filter(
            owner=self.request.user
        )
        return form


class ExpenseCategoryListView(LoginRequiredMixin, ListView):
    model = ExpenseCategory
    template_name = "expenses/list.html"
    context_object_name = "expense_list"

    def get_queryset(self):
        return ExpenseCategory.objects.filter(owner=self.request.user)


class AccountListView(ListView):
    model = Account
    template_name = "accounts/list.html"
    context_object_name = "account_list"

    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user)


class ExpenseCategoryCreateView(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    fields = ["name"]
    template_name = "expenses/new.html"
    success_url = reverse_lazy("category_list")

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super().form_valid(form)


class AccountCreateView(CreateView):
    model = Account
    fields = ["name", "number"]
    template_name = "accounts/new.html"
    success_url = reverse_lazy("account_list")

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super().form_valid(form)
